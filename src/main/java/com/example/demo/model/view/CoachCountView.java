package com.example.demo.model.view;

public class CoachCountView {

    private Integer countCoaches;

    public CoachCountView() {
    }

    public CoachCountView(Integer countCoaches) {
        this.countCoaches = countCoaches;
    }

    public Integer getCountCoaches() {
        return countCoaches;
    }

    public CoachCountView setCountCoaches(Integer countCoaches) {
        this.countCoaches = countCoaches;
        return this;
    }
}
