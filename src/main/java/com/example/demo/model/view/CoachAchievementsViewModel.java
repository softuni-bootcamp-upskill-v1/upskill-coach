package com.example.demo.model.view;

import java.time.LocalDateTime;

public class CoachAchievementsViewModel {
    private String id;
    private String name;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Integer session;

    public CoachAchievementsViewModel() {
    }

    public String getId() {
        return id;
    }

    public CoachAchievementsViewModel setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CoachAchievementsViewModel setName(String name) {
        this.name = name;
        return this;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public CoachAchievementsViewModel setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public CoachAchievementsViewModel setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public Integer getSession() {
        return session;
    }

    public CoachAchievementsViewModel setSession(Integer session) {
        this.session = session;
        return this;
    }
}
