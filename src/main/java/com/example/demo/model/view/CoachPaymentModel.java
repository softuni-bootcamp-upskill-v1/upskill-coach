package com.example.demo.model.view;

import java.math.BigDecimal;

public class CoachPaymentModel {

    private BigDecimal price;
    private String coachId;
    private String name;

    public String getBusinessOwnerId() {
        return businessOwnerId;
    }

    public CoachPaymentModel setBusinessOwnerId(String businessOwnerId) {
        this.businessOwnerId = businessOwnerId;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CoachPaymentModel setEmail(String email) {
        this.email = email;
        return this;
    }

    private String businessOwnerId;
    private String email;


    public String getCoachId() {
        return coachId;
    }

    public CoachPaymentModel setCoachId(String coachId) {
        this.coachId = coachId;
        return this;
    }

    public String getName() {
        return name;
    }

    public CoachPaymentModel setName(String name) {
        this.name = name;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public CoachPaymentModel setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public CoachPaymentModel() {
    }
}
